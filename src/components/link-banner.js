import React from 'react';

const LinkBanner = (props) => {
  return (
    <div id="link-banner">
      <button id="about-link">About Me</button>
      <button id="project-link">Projects</button>
    </div>
  );
}
export default LinkBanner;