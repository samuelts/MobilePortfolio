import React from 'react';

const Contact = () => {
  return (
    <div id="contact-me">
      <div class="contact-row">
        <a href="https://www.linkedin.com/in/stephsam/" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/linkedin.svg" class="contact-icons"/>
          <div class="contact-labels">LinkedIn</div>
        </a>
        <a id="profile-link" href="https://github.com/safwyls" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/github.svg" class="contact-icons"/>
          <div class="contact-labels">GitHub</div>
        </a>
        <a href="https://www.freecodecamp.org/safwyl" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/freecodecamp.svg" class="contact-icons"/>
          <div class="contact-labels">FreeCodeCamp</div>
        </a>
        <a href="https://codepen.io/safwyl/" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/codepen.svg" class="contact-icons"/>
          <div class="contact-labels">Codepen</div>
        </a>
        <a href="mailto:samueltstephenson@gmail.com">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/gmail.svg" class="contact-icons"/>
          <div class="contact-labels">Gmail</div>
        </a>
      </div>
    </div>
  );
}
export default Contact;