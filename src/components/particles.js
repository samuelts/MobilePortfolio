import React, { Component } from 'react';
import Particles from 'react-particles-js';
import detectIt from 'detect-it';

class IdleAnim extends Component {
  constructor(props) {
    super(props);

    const isTouch = detectIt.primaryInput === 'touch' ? true : false;
    const windowWidth = window.screen.availWidth;

    this.state = {
      params: {
        "particles": {
          "number": {
            "value": windowWidth < 500 ? 10 : 50,
            "density": {
              "enable": true,
              "value_area": windowWidth < 500 ? 800 : 500
            }
          },
          "color": {
            "value": "#50fa7b"
          },
          "shape": {
            "type": "circle",
            "stroke": {
              "width": 0,
              "color": "#000000"
            },
            "polygon": {
              "nb_sides": 5
            },
            "image": {
              "src": "img/github.svg",
              "width": 100,
              "height": 100
            }
          },
          "opacity": {
            "value": 0.4,
            "random": false,
            "anim": {
              "enable": false,
              "speed": 1,
              "opacity_min": 0.1,
              "sync": false
            }
          },
          "size": {
            "value": 3,
            "random": true,
            "anim": {
              "enable": false,
              "speed": 40,
              "size_min": 0.1,
              "sync": false
            }
          },
          "line_linked": {
            "enable": true,
            "distance": 150,
            "color": "#8be9fd",
            "opacity": 0.4,
            "width": 1
          },
          "move": {
            "enable": true,
            "speed": 6,
            "direction": "none",
            "random": false,
            "straight": false,
            "out_mode": "out",
            "bounce": false,
            "attract": {
              "enable": false,
              "rotateX": 600,
              "rotateY": 1200
            }
          }
        },
        "interactivity": {
          "detect_on": "canvas",
          "events": {
            "onhover": {
              "enable": isTouch ? false : true,
              "mode": "bubble"
            },
            "onclick": {
              "enable": false,
              "mode": "repulse"
            },
            "resize": true
          },
          "modes": {
            "grab": {
              "distance": 400,
              "line_linked": {
                "opacity": 1
              }
            },
            "bubble": {
              "distance": 400,
              "size": 5,
              "duration": 2,
              "opacity": 1,
              "speed": 3
            },
            "repulse": {
              "distance": 100,
              "duration": 0.4
            },
            "push": {
              "particles_nb": 4
            },
            "remove": {
              "particles_nb": 2
            }
          }
        },
        "retina_detect": true
      }
    };
  }

  shouldComponentUpdate() {
    return false;
  }

  render() {

    return (
      <Particles
        params={this.state.params}
        width="100%"
        height="100%"
        className="particles-wrap"
        canvasClassName="particles-js"
        style={{
          position: "absolute",
          top: "0",
          left: "0",
          overflow: "hidden"
          }}
      />
    );
  }
}
export default IdleAnim;