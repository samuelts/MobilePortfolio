import React from 'react';

const About = () => {
  return (
    <div id="about-me">
      <div id="intro">
        Hi! My name is Sam and I'm a web developer. I currently focus on frontend development but am expanding into full stack.
      </div>
      <div class="divider">
          <i class="fas fa-minus"></i>
      </div>
      <div id="techs-start">
        Here are some of the technologies I work with
      </div>
      <div id="techs">
        <div class="tech-row">
        <a href="https://github.com/safwyls?utf8=%E2%9C%93&tab=repositories&q=%23html" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/html5.svg" class="tech-icons"/>
          <div class="tech-labels">HTML5</div>
        </a>
        <a href="https://github.com/safwyls?utf8=%E2%9C%93&tab=repositories&q=%23css" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/css3.svg" class="tech-icons"/>
          <div class="tech-labels">CSS3</div>
        </a>
        <a href="https://github.com/safwyls?utf8=%E2%9C%93&tab=repositories&q=%23javascript" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/javascript.svg" class="tech-icons"/>
          <div class="tech-labels">JS</div>
        </a>
        </div>
        <div class="tech-row">
        <a href="https://github.com/safwyls?utf8=%E2%9C%93&tab=repositories&q=%23react" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/react.svg" class="tech-icons"/>
          <div class="tech-labels">React</div>
        </a>
        <a href="https://github.com/safwyls?utf8=%E2%9C%93&tab=repositories&q=%23redux" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/redux.svg" class="tech-icons"/>
          <div class="tech-labels">Redux</div>
        </a>
        <a href="https://github.com/safwyls?utf8=%E2%9C%93&tab=repositories&q=%23bootstrap" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/bootstrap.svg" class="tech-icons"/>
          <div class="tech-labels">Bootstrap</div>
        </a>
        <a href="https://github.com/safwyls?utf8=%E2%9C%93&tab=repositories&q=%23jquery" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/jquery.svg" class="tech-icons"/>
          <div class="tech-labels">jQuery</div>
        </a>
        </div>
        <div class="tech-row">
        <a href="https://github.com/safwyls?utf8=%E2%9C%93&tab=repositories&q=%23node" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/nodejs.svg" class="tech-icons"/>
          <div class="tech-labels">node.js</div>
        </a>
        <a href="https://github.com/safwyls?utf8=%E2%9C%93&tab=repositories&q=%23python" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/python.svg" class="tech-icons"/>
          <div class="tech-labels">Python</div>
        </a>
        <a href="https://github.com/safwyls?utf8=%E2%9C%93&tab=repositories&q=%23django" target="_blank">
          <img src="https://cdn.jsdelivr.net/npm/simple-icons@latest/icons/django.svg" class="tech-icons"/>
          <div class="tech-labels">Django</div>
        </a>
        </div>
      </div>
    </div>
  );
}
export default About;