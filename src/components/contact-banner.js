import React from 'react';

const ContactBanner = (props) => {
  return (
    <div id="contact-banner">
      <button id="contact-link" onClick={props.contactInteract}>{props.contactVis ? 'Back' : 'Contact Me'}</button>
    </div>
  );
}
export default ContactBanner;