import React, { Component } from 'react';
import TopBanner from './top-banner';
import ContactBanner from './contact-banner';
import LinkBanner from './link-banner';
import IdleAnim from './particles';
import About from './about';
import Projects from './projects';
import Contact from './contact';

export default class App extends Component {
  constructor (props) {
    super(props);

    this.state = {
      contentWrapVis: false,
      aboutVis: false,
      projectsVis: false,
      contactVis: false,
      particleVis: true
    };

    this.handleInteract = this.handleInteract.bind(this);
    this.contactInteract = this.contactInteract.bind(this);
  }

  handleInteract() {
    this.setState({
      aboutVis: true
    });
  }

  contactInteract() {
    this.setState({
      contactVis: !this.state.contactVis
    });
  }

  render() {
    const wrapStyle = this.state.aboutVis === false &&
                      this.state.projectsVis === false &&
                      this.state.contactVis === false ?
                        {pointerEvents: "none"} :
                        {};

    return (
      <div className='container'>
        <IdleAnim />
        <TopBanner />
        <LinkBanner />
        <div className='content-wrap' style={wrapStyle}>
          { this.state.aboutVis ? <About /> : null }
          { this.state.projectsVis ? <Projects /> : null }
          { this.state.contactVis ? <Contact /> : null }
        </div>
        <ContactBanner contactVis={this.state.contactVis} contactInteract={this.contactInteract}/>
      </div>
    );
  }
}
