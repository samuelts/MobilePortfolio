import React from 'react';

const TopBanner = () => {
  return (
    <div id="top-banner">
      <div id="name">Samuel Stephenson</div>
      <div id="subtitle">Web Dev, Designer, Engineer</div>
    </div>
  );
}
export default TopBanner;